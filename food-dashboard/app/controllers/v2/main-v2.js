import {
  layThongTinTuForm,
  renderDanhSachMonAn,
  showMonAnLenForm,
} from "./controller-v2.js";

let danhSachMonAn = [];

document.getElementById("btnThemMon").addEventListener("click", function () {
  let monAn = layThongTinTuForm();
  // console.log("monAn: ", monAn);
  danhSachMonAn.push(monAn);
  renderDanhSachMonAn(danhSachMonAn);
});

function xoaMonAn(id) {
  // console.log("id: ", id);
  let index = danhSachMonAn.findIndex((item) => {
    return item.ma == id;
  });
  // console.log("index: ", index);
  if (index !== -1) {
    danhSachMonAn.splice(index, 1);
    renderDanhSachMonAn(danhSachMonAn);
  }
}

window.xoaMonAn = xoaMonAn;

function suaMonAn(id) {
  console.log("id: ", id);
  // b1 tìm ra index
  // b2 gọi hàm showMonAnLenForm từ controller

  let index = danhSachMonAn.findIndex((item) => {
    return item.ma == id;
  });

  if (index !== -1) {
    showMonAnLenForm(danhSachMonAn[index]);
  }
}
window.suaMonAn = suaMonAn;

import { MonAnV2 } from "../../models/v2/monAnModel-v2.js";

export function layThongTinTuForm() {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let gia = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  let monAn = new MonAnV2(
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  return monAn;
}

export function showMonAnLenForm(monAn) {
  document.getElementById("foodID").value = monAn.ma;
  document.getElementById("tenMon").value = monAn.ten;
  document.getElementById("loai").value = monAn.loai;
  document.getElementById("giaMon").value = monAn.gia;
  document.getElementById("khuyenMai").value = monAn.phanTramKm;
  document.getElementById("tinhTrang").value = monAn.tinhTrang;
  document.getElementById("hinhMon").value = monAn.hinhMon;
  document.getElementById("moTa").value = monAn.moTa;
}

export function renderDanhSachMonAn(foodList) {
  let contentHTML = "";
  foodList.forEach((item) => {
    let contenTr = `
     <tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.loai == "chay" ? "Chay" : "Mặn"}</td>
        <td>${item.gia}</td>
        <td>${item.phanTramKm * 100} %</td>
        <td>${item.tinhGiaKM()}</td>
        <td>${item.tinhTrang == 0 ? "Hết " : "Còn"}</td>
        <td>
        <button onclick="xoaMonAn('${
          item.ma
        }')" class="btn btn-danger">Xoá</button>
        <button
        onclick="suaMonAn('${item.ma}')"
        data-toggle="modal"
        data-target="#exampleModal"
        class="btn btn-warning">Sửa</button>
        </td>
     </tr>
     `;
    contentHTML += contenTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
}

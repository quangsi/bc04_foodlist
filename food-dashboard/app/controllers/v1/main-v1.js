import { MonAnV1 } from "../../models/v1/monAnModel-v1.js";
function layThongTinTuForm() {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let gia = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  let monAn = new MonAnV1(
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  return monAn;
}

let showMonAnLenForm = (monAn) => {
  let { ma, ten, gia, phanTramKm, tinhTrang, hinhMon, moTa, loai } = monAn;

  document.getElementById("spMa").innerText = ma;
  document.getElementById("spTenMon").innerText = ten;
  document.getElementById("spLoaiMon").innerText = loai;
  document.getElementById("spGiaKM").innerText = monAn.tinhGiaKM();
};
document.getElementById("btnThemMon").addEventListener("click", function () {
  let monAn = layThongTinTuForm();
  console.log("monAn: ", monAn);
  showMonAnLenForm(monAn);
});

import { username, isHoliday } from "./utils.js";

import isThuBay from "./utils.js";

console.log(username, isHoliday, isThuBay);

function Dog(name) {
  this.name = name;
}

let milu = new Dog("milu");

class Cat {
  constructor(name) {
    this.name = name;
  }
  sayHello() {}
}
let meo = new Cat("Mèo");
